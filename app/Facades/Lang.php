<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 20.11.15
 * Time: 8:10
 */

namespace App\Facades;

class Lang extends \Illuminate\Support\Facades\Lang
{
    /**
     * Returns capitalized the first letter of the string.
     *
     * @param String $str
     * @return String
     */
    public static function ucFirst($str)
    {
        $strResource = self::get($str);

        return mb_strtoupper(mb_substr($strResource, 0, 1)) . mb_substr($strResource, 1);
    }
}