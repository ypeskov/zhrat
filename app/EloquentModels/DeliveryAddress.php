<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryAddress extends Model
{
    public function users()
    {
        return $this->belongsToMany('Models\User');
    }
}
