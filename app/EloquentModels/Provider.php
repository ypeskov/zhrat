<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public function user()
    {
        return $this->belongsTo('Models\User');
    }
}
