<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 12.11.15
 * Time: 21:12
 */

namespace Models;

use App\User as BaseUser;

/**
 * Class User
 *
 * This class is used for adding extra functionality to Laravel's User class
 * and keep it in one place in EloquentModels namespace.
 *
 * @package App\EloquentModels
 */
class User extends BaseUser
{
    public function roles()
    {
        return $this->belongsToMany('Models\Role');
    }

    public function provider()
    {
        return $this->hasOne('Models\Provider');
    }

    public function deliveryAddresses()
    {
        return $this->belongsToMany('Models\DeliveryAddress');
    }
}