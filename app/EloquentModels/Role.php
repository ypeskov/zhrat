<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 11.11.15
 * Time: 15:45
 */

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table    = 'roles';

    public function users()
    {
        return $this->belongsToMany('Models\User');
    }
}