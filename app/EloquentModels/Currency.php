<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public function meals()
    {
        return $this->hasMany('Models\Meal');
    }
}
