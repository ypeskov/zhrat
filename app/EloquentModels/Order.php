<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function orderStatus()
    {
        return $this->belongsTo('Models\OrderStatus');
    }
}
