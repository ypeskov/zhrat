<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    public function currency()
    {
        return $this->belongsTo('Models\Currency');
    }
}
