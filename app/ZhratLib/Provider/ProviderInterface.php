<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 18.11.15
 * Time: 13:46
 */

namespace ZhratLib\Provider;

use Models\User;

interface ProviderInterface
{
    public function isProvider(User $user);

}