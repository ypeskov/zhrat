<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 18.11.15
 * Time: 13:44
 */

namespace ZhratLib\Provider;

use Illuminate\Support\ServiceProvider;

class ProviderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('ZhratLib\Provider\ProviderInterface', function() {
            return new ProviderRepository();
        });
    }
}