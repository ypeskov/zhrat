<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 18.11.15
 * Time: 13:46
 */

namespace ZhratLib\Provider;

use Models\User;

class ProviderRepository implements ProviderInterface
{
    const MEAL_PROVIDER_CODE = 'provider';

    private $isMealProvider = false;

    /**
     * Return whether the user has provider role.
     *
     * @param User $user
     * @return bool
     */
    public function isProvider(User $user)
    {
        $user->load('roles');

        foreach($user->roles as $role) {
            if ( $role->code === self::MEAL_PROVIDER_CODE ) {
                $this->isMealProvider = true;

                return $this->isMealProvider;
            }
        }

        return $this->isMealProvider;
    }
}