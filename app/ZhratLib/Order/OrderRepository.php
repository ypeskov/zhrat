<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 18.11.15
 * Time: 11:42
 */

namespace ZhratLib\Order;

use App\Facades\Lang;
use Models\Order;
use Models\Provider;

class OrderRepository implements OrderInterface
{

    public function getAllOrders()
    {
        return Order::all();
    }

    public function getActiveOrdersForProvider(Provider $provider)
    {
        $allOrders =  Order::where('provider_id', $provider->id)->get();

        return $this->prepareStructuredOrders($this->prepareOrdersStructure(), $allOrders);
    }

    /**
     * Processes all orders and build the array with order types.
     *
     * @param array $structure
     * @param \Traversable $allOrders
     * @return array
     */
    private function prepareStructuredOrders(Array $structure, \Traversable $allOrders)
    {
        foreach($allOrders as $order) {
            if ( $order->orderStatus->status_code === self::ORDER_STATUS_ORDERED ) {
                $structure['ordered']['orders'][] = $order->toArray();
                $structure['ordered']['quantity'] += 1;
            }

            if ( $order->orderStatus->status_code === self::ORDER_STATUS_ACCEPTED ) {
                $structure['accepted']['orders'][] = $order->toArray();
                $structure['accepted']['quantity'] += 1;
            }

            if ( $order->orderStatus->status_code === self::ORDER_STATUS_IN_KITCHEN ) {
                $structure['in_kitchen']['orders'][] = $order->toArray();
                $structure['in_kitchen']['quantity'] += 1;
            }

            if ( $order->orderStatus->status_code === self::ORDER_STATUS_IN_DELIVERY ) {
                $structure['in_delivery']['orders'][] = $order->toArray();
                $structure['in_delivery']['quantity'] += 1;
            }

            if ( $order->orderStatus->status_code === self::ORDER_STATUS_READY_FOR_DELIVERY ) {
                $structure['ready_for_delivery']['orders'][] = $order->toArray();
                $structure['ready_for_delivery']['quantity'] += 1;
            }

            if ( $order->orderStatus->status_code === self::ORDER_STATUS_WAIT_FOR_COURIER ) {
                $structure['wait_for_courier']['orders'][] = $order->toArray();
                $structure['wait_for_courier']['quantity'] += 1;
            }
        }

        return $structure;
    }

    /**
     * Return the empty structure of orders as an array.
     *
     * @return array
     */
    private function prepareOrdersStructure()
    {
        return [
            'ordered' => [
                'label'     => Lang::ucFirst('app.new_orders'),
                'orders'    => [],
                'quantity'  => 0,
            ],

            'accepted' => [
                'label'     => Lang::ucFirst('app.accepted_orders'),
                'orders'    => [],
                'quantity'  => 0,
            ],

            'in_kitchen' => [
                'label'     => Lang::ucFirst('app.orders_in_kitchen'),
                'orders'    => [],
                'quantity'  => 0,
            ],

            'ready_for_delivery' => [
                'label'     => Lang::ucFirst('app.orders_ready_for_delivery'),
                'orders'    => [],
                'quantity'  => 0,
            ],

            'wait_for_courier' => [
                'label'     => Lang::ucFirst('app.orders_waiting_for_courier'),
                'orders'    => [],
                'quantity'  => 0,
            ],

            'in_delivery' => [
                'label'     => Lang::ucFirst('app.orders_in_delivery'),
                'orders'    => [],
                'quantity'  => 0,
            ],
        ];
    }
}