<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 18.11.15
 * Time: 11:44
 */
namespace ZhratLib\Order;

use Models\Provider;

interface OrderInterface
{
    const ORDER_STATUS_ORDERED              = 'ordered';
    const ORDER_STATUS_ACCEPTED             = 'accepted_by_provider';
    const ORDER_STATUS_IN_KITCHEN           = 'in_kitchen';
    const ORDER_STATUS_READY_FOR_DELIVERY   = 'ready_for_delivery';
    const ORDER_STATUS_WAIT_FOR_COURIER     = 'wait_for_courier';
    const ORDER_STATUS_IN_DELIVERY          = 'in_delivery';
    const ORDER_STATUS_DELIVERED            = 'delivered';
    const ORDER_STATUS_SUSPENDED            = 'suspended';
    const ORDER_STATUS_CANCELED             = 'canceled';

    public function getAllOrders();

    public function getActiveOrdersForProvider(Provider $provider);
}