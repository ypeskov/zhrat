<?php

namespace ZhratLib\Order;

use Illuminate\Support\ServiceProvider;


class OrderServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ZhratLib\Order\OrderInterface', function(){

            return new OrderRepository();

        });
    }

}
