<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::pattern('subDomain', '(provider|admin)');
Route::pattern('domain', '(zhrat-and-pit.local|zhrat-and-pit.com)');

Route::group(['domain' => '{subDomain}.{domain}'], function () {
    // Authentication routes...
    Route::get('auth/login', 'Provider\Auth\AuthController@getLogin');
    Route::post('auth/login', 'Provider\Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Provider\Auth\AuthController@getLogout');

    // Registration routes...
    Route::get('auth/register', 'Provider\Auth\AuthController@getRegister');
    Route::post('auth/register', 'Provider\Auth\AuthController@postRegister');


    Route::group([
            'middleware'    => 'auth',
        ],
        function() {
            Route::get('/', [
                'as'    => 'mainProviderRoute',
                'uses'  => 'Provider\DashboardController@index',
            ]);
        }
    );

});

Route::get('/', function () {
    return 'Domain: No Sub Domain';
});



