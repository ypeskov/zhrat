<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 12.11.15
 * Time: 21:27
 */

namespace App\Http\Controllers\Provider;

use \Response;
use \Auth;

use ZhratLib\Order\OrderInterface;
use ZhratLib\Provider\ProviderInterface;

class DashboardController extends BaseProviderController
{
    private $orderRepo;

    public function __construct(OrderInterface $orderRepo, ProviderInterface $provider)
    {
        parent::__construct($provider);

        $this->orderRepo    = $orderRepo;
    }

    public function index()
    {
        $data = [
            'user'      => $this->advancedUser,
            'orders'    => $this
                                ->orderRepo
                                ->getActiveOrdersForProvider($this->advancedUser->provider),
        ];

        return Response::view('provider.dashboard.dashboard', $data);
    }
}