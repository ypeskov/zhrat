<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 18.11.15
 * Time: 14:32
 */

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use \Auth;

use ZhratLib\Provider\ProviderInterface;

class BaseProviderController extends Controller
{
    protected $provider;
    protected $isProvider = false;

    protected $advancedUser;

    public function __construct(ProviderInterface $provider)
    {
        $this->provider     = $provider;
        $this->advancedUser = Auth::user()->getAdvancedUser();

        if ( !$this->isProvider() ) {
            throw new \Exception('Access Denied', 403);
        }
    }

    /**
     * check if the current logged user has provider role.
     *
     * @return boolean
     */
    protected function isProvider()
    {
        $this->isProvider = $this
                                ->provider
                                ->isProvider($this->advancedUser);

        return $this->isProvider;
    }
}