var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

//elixir(function(mix) {
//    mix.sass('app.scss');
//});

elixir(function(mix) {
    mix.less([
        'provider.less'
    ])
});

elixir(function(mix) {
    mix.styles(
        ['bootstrap/dist/css/bootstrap.min.css'],

        'public/css/vendors.css',
        'resources/assets/vendor'
    );
});

elixir(function(mix) {
    mix.scripts(
        ['jquery/dist/jquery.min.js',
            'bootstrap/dist/js/bootstrap.min.js'],

        'public/js/vendors.js',
        'resources/assets/vendor'
    );
});