<?php

use Illuminate\Database\Seeder;
use Models\Provider;
use Models\User;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provider::create([
            'provider_full_name'    => 'У Якова',
            'description'           => 'мега супер кафешка',
            'address_city'          => 'где-то в Яремче',
            'address_street'        => 'fake street 23',
            'contact_person'        => 'Yakov Petrovich',
            'contact_phone1'        => '+380 50 123-45-67',
            'user_id'               => User::where('email', 'yuriy.peskov@gmail.com')->first()->id,
        ]);

        Provider::create([
            'provider_full_name'    => 'Black Sea',
            'description'           => 'cafe & hotel',
            'address_city'          => 'Odessa',
            'address_street'        => 'fake street 24',
            'contact_person'        => 'Yakov Petrovich',
            'contact_phone1'        => '+380 50 123-45-67',
            'user_id'               => User::where('email', 'ypeskov@pisem.net')->first()->id,
        ]);
    }
}
