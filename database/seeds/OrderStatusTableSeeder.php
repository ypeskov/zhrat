<?php

use Illuminate\Database\Seeder;
use Models\OrderStatus;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'status_name'           => 'ordered',
            'status_code'           => 'ordered',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'canceled',
            'status_code'           => 'canceled',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'accepted by provider',
            'status_code'           => 'accepted_by_provider',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'in process of manufacturing',
            'status_code'           => 'in_kitchen',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'ready for delivery',
            'status_code'           => 'ready_for_delivery',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'waiting for courier',
            'status_code'           => 'wait_for_courier',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'in delivery',
            'status_code'           => 'in_delivery',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'delivered',
            'status_code'           => 'delivered',
            'status_description'    => 'status can be used for assigning orders',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'disabled',
            'status_code'           => 'disabled',
            'status_description'    => 'status is not available',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'suspended',
            'status_code'           => 'suspended',
            'status_description'    => 'status is suspended at this moment',
            'is_enabled_to_use'     => 1,
        ]);

        OrderStatus::create([
            'status_name'           => 'future_test',
            'status_code'           => 'future_test',
            'status_description'    => 'status is used for test purposes',
            'is_enabled_to_use'     => 0,
        ]);
    }
}
