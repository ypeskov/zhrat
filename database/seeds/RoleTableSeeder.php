<?php

use Illuminate\Database\Seeder;
use Models\Role;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'code'          => 'client',
            'label'         => 'client',
            'description'   => 'Service client',
        ]);

        Role::create([
            'code'          => 'provider',
            'label'         => 'provider',
            'description'   => 'Provider of meals',
        ]);

        Role::create([
            'code'          => 'super_admin',
            'label'         => 'super admin',
            'description'   => 'Admin with full permissions',
        ]);
    }
}
