<?php

use Illuminate\Database\Seeder;
use Models\Meal;
use Models\MealOrder;
use Models\Order;

class MealOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = Order::find(1);
        $meals = [
            1 => Meal::find(1),
            2 => Meal::find(2),
            3 => Meal::find(3),
        ];
        $this->addOrderMeals($order, $meals);

        $order = Order::find(2);
        $meals = [
            1 => Meal::find(2),
            2 => Meal::find(3),
        ];
        $this->addOrderMeals($order, $meals);

        $order = Order::find(3);
        $meals = [
            1 => Meal::find(2),
            2 => Meal::find(4),
        ];
        $this->addOrderMeals($order, $meals);
    }

    private function addOrderMeals($order, $meals)
    {
        foreach($meals as $meal) {
            MealOrder::create([
                'order_id'      => $order->id,
                'price'         => $meal->price,
                'name'          => $meal->name,
                'weight'        => $meal->weight,
            ]);
        }

        $orderMeals = MealOrder::where('order_id', (int) $order->id)->get();
        $orderAmount = 0;
        foreach($orderMeals as $meal) {
            $orderAmount += (double) $meal->price;
        }

        $order->order_amount = $orderAmount;
        $order->save();
    }
}
