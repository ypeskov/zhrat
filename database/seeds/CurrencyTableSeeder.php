<?php

use Illuminate\Database\Seeder;
use Models\Currency;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create([
            'name'          => 'Hryvnia',
            'code_symbol'   => 'UAH',
            'code_number'   => '980',
            'description'   => 'ukrainian hryvna',
            'country_name'  => 'Ukraine',
            'minor_unit'    => 2,
        ]);

        Currency::create([
            'name'          => 'US Dollar',
            'code_symbol'   => 'USD',
            'code_number'   => '840',
            'description'   => 'USA Dollar',
            'country_name'  => 'United States of America',
            'minor_unit'    => 2,
        ]);
    }
}
