<?php

use Illuminate\Database\Seeder;
use Models\User;
use Models\Role;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        //insert test clients

        $clientRole = Role::where('code', 'client')->first();

        $user = User::create([
            'first_name'    => 'Yuriy',
            'middle_name'   => '',
            'last_name'     => 'Peskov',
            'email'         => 'yuriy.peskov@gmail.com',
            'password'      => Hash::make('123'),
        ]);
        $user->roles()->attach($clientRole);

        $user = User::create([
            'first_name'    => 'Yuriy',
            'middle_name'   => '',
            'last_name'     => 'Cafe',
            'email'         => 'ypeskov@pisem.net',
            'password'      => Hash::make('123'),
        ]);
        $user->roles()->attach(Role::where('code', 'provider')->first());

        $user = User::create([
            'first_name'    => 'Eugene',
            'middle_name'   => '',
            'last_name'     => 'Zhurakhovskiy',
            'email'         => 'txc.work@gmail.com',
            'password'      => Hash::make('123'),
        ]);
        $user->roles()->attach($clientRole);
    }
}
