<?php

use Illuminate\Database\Seeder;
use Models\User;
use Models\DeliveryAddress;


class DeliveryAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::where('email', 'yuriy.peskov@gmail.com')->firstOrFail();
        $user2 = User::where('email', 'txc.work@gmail.com')->firstOrFail();

        $da = DeliveryAddress::create([
            'delivery_name'     => 'AB Soft',
            'country'           => 'Ukraine',
            'city'              => 'Odessa',
            'street'            => 'вице адмирала Жукова 21/23',
            'contact_name'      => 'Olga',
            'contact_phone'     => '123456',
            'comments'          => 'bla bla bla',
        ]);
        $da->users()->attach([$user1->id, $user2->id]);


        $da = DeliveryAddress::create([
            'delivery_name'     => 'Юра домашний',
            'country'           => 'Ukraine',
            'city'              => 'Odessa',
            'street'            => 'ул. академика Королева 42, кв. 38',
            'contact_name'      => 'Юра',
            'contact_phone'     => '123456',
            'comments'          => 'УЛАЛАЛА. ОЛОЛОЛО',
        ]);
        $da->users()->attach($user1);
    }
}
