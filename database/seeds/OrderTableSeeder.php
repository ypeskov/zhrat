<?php

use Illuminate\Database\Seeder;
use Models\Order;
use Models\Provider;
use Models\User;
use Models\OrderStatus;
use Models\Currency;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orderStatus = OrderStatus::where('status_code', 'ordered')->first();
        $provider = Provider::find(2);

        $user = User::where('email', 'ypeskov@pisem.net')->firstOrFail();
        $this->addOrder($orderStatus, $user, $provider, 12);

        $user = User::where('email', 'yuriy.peskov@gmail.com')->firstOrFail();
        $this->addOrder($orderStatus, $user, $provider, 12);

        $user = User::where('email', 'txc.work@gmail.com')->firstOrFail();
        $this->addOrder($orderStatus, $user, $provider, 13);
    }

    private function addOrder(OrderStatus $orderStatus,
                              User $user, Provider $provider, $hour,
                              $currencySymbol='UAH')
    {
        $order = Order::create([
            'provider_id'           => $provider->id,
            'user_id'               => $user->id,
            'order_status_id'       => $orderStatus->id,
            'currency_code_symbol'  => $currencySymbol,
            'delivery_datetime'     => $this->getTomorrowDateTime($hour),
            'delivery_timezone'     => 'Europe/Kiev',
        ]);

        $order->order_code = 'user-' . $user->id . '/order-' . $order->id;
        $order->save();
    }

    private function getTomorrowDateTime($hour)
    {
        $now = (new DateTime())->format('Y-m-d');

        return (new DateTime($now . ' ' . $hour . ':00'))->modify('+1 day');
    }
}
