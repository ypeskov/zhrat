<?php

use Illuminate\Database\Seeder;
use Models\Provider;
use Models\Currency;
use Models\Meal;

class MealTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currency = Currency::where('code_symbol', 'UAH')->first();
        $provider = Provider::find(1)->first();

        Meal::create([
            'name'          => 'Potato free',
            'weight'        => 200,
            'price'         => 19.50,
            'currency_id'   => $currency->id,
            'provider_id'   => $provider->id,
        ]);

        Meal::create([
            'name'          => 'Компот',
            'weight'        => 200,
            'price'         => 5.50,
            'currency_id'   => $currency->id,
            'provider_id'   => $provider->id,
        ]);

        Meal::create([
            'name'          => 'Пельмени',
            'weight'        => 200,
            'price'         => 25.50,
            'currency_id'   => $currency->id,
            'provider_id'   => $provider->id,
        ]);

        Meal::create([
            'name'          => 'Вареники с вишней',
            'weight'        => 200,
            'price'         => 30,
            'currency_id'   => $currency->id,
            'provider_id'   => $provider->id,
        ]);
    }
}
