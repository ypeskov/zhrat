<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    const TABLE_ROLES_NAME  = 'roles';
    const CODE_LENGTH       = 30;
    const LABEL_LENGTH      = 30;
    const DESCRIPTION_LENGTH= 100;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_ROLES_NAME, function(Blueprint $table) {
            $table->increments('id');

            $table->string('code', self::CODE_LENGTH)->unique();
            $table->string('label', self::LABEL_LENGTH)->unique();
            $table->string('description', self::DESCRIPTION_LENGTH);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_ROLES_NAME);
    }
}
