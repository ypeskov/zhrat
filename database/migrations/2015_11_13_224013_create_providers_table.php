<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    const TABLE_NAME = 'providers';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');

            $table->string('provider_full_name', 100);
            $table->string('description', 200);
            $table->string('address_city', 100);
            $table->string('address_street', 200);
            $table->string('contact_person', 200);
            $table->string('contact_phone1', 45);
            $table->string('contact_phone2', 45);
            $table->string('comments', 500);

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
