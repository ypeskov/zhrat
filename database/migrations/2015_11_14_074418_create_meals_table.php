<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealsTable extends Migration
{
    const TABLE_NAME = 'meals';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');

            $table->string('name', 50);
            $table->integer('weight');
            $table->decimal('price', 12, 3);

            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')
                ->references('id')
                ->on(CreateCurrenciesTable::TABLE_NAME)
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')
                ->references('id')
                ->on(CreateProvidersTable::TABLE_NAME)
                ->onDelete('cascade')
                ->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
