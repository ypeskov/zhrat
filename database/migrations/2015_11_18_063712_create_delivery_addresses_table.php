<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryAddressesTable extends Migration
{
    const TABLE_NAME = 'delivery_addresses';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');

            $table->string('delivery_name', 45)->unique();
            $table->string('country', 45);
            $table->string('city', 45);
            $table->string('street', 45);
            $table->string('street2', 45);
            $table->string('contact_name', 45);
            $table->string('contact_phone', 45);
            $table->string('comments', 100);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
