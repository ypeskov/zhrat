<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    const TABLE_NAME = 'currencies';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 45);

            //code symbol and number should be stored according to ISO 4217
            $table->string('code_symbol', 3)->unique();
            $table->string('code_number', 3)->unique();

            $table->string('description', 150);
            $table->string('country_name', 50)->unique();

            $table->tinyInteger('minor_unit');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
