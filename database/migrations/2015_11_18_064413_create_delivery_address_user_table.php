<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryAddressUserTable extends Migration
{
    const TABLE_NAME = 'delivery_address_user';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');

            $table->integer('delivery_address_id')->index()->unsigned();
            $table->foreign('delivery_address_id')
                ->references('id')
                ->on(CreateDeliveryAddressesTable::TABLE_NAME)
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on(CreateUsersTable::TABLE_NAME)
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
