<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    const TABLE_NAME = 'orders';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table) {
            $table->increments('id');

            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')
                ->references('id')
                ->on(CreateProvidersTable::TABLE_NAME);

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on(CreateUsersTable::TABLE_NAME);

            $table->decimal('order_amount', 12, 3);
            $table->string('currency_code_symbol', 3);
            $table->dateTime('delivery_datetime');
            $table->string('delivery_timezone', 45);

            $table->string('order_code', 45);

            $table->string('delivery_address', 150);

            $table->integer('order_status_id')->unsigned();
            $table->foreign('order_status_id')
                ->references('id')
                ->on(CreateOrderStatusTable::TABLE_NAME);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
