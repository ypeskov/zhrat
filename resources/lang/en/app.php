<?php

return [
    'logout'    => 'logout',
    'email'     => 'email',
    'provider'  => 'provider',

    'home'      => 'home',
    'orders'    => 'orders',

    'new_orders'        => 'new orders',
    'accepted_orders'   => 'accepted orders',
    'orders_in_kitchen' => 'orders in kitchen',
    'orders_in_delivery'=> 'orders in delivery',
    'orders_ready_for_delivery' => 'orders ready for delivery',
    'orders_waiting_for_courier'=> 'orders waiting for courier',


];