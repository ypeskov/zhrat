@extends('provider.layouts.main')

@section('content')

    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form method="POST" action="/auth/register">
    {!! csrf_field() !!}

        <div>
            {{ Lang::get('first_name') }}
            <input type="text" name="first_name" value="{{ old('first_name') }}">
        </div>

        <div>
            {{ Lang::get('last_name') }}
            <input type="text" name="last_name" value="{{ old('last_name') }}">
        </div>

        <div>
            {{ Lang::get('email') }}
            <input type="email" name="email" value="{{ old('email') }}">
        </div>

        <div>
            {{ Lang::get('password') }}
            <input type="password" name="password">
        </div>

        <div>
            {{ Lang::get('password_confirmation') }}
            <input type="password" name="password_confirmation">
        </div>

        <div>
            <button type="submit">{{ Lang::get('register') }}</button>
        </div>
    </form>
@stop

