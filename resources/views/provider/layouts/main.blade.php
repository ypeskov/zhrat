<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">

    <title>{{ Lang::get('Zhrat-and-Pit manager') }}</title>

    <link rel="stylesheet" href="/css/vendors.css" type="text/css">
    <link rel="stylesheet" href="/css/app.css" type="text/css">

</head>

<body>
    <div id="page_wrapper">

        <div id="content_wrapper" class="container">
            @yield('content')
        </div>

        @section('footer')
            <div id="footer_wrapper" class="container">
                contact: zhrat@gmail.com
            </div>
        @show
    </div>

    <script type="text/javascript" src="/js/vendors.js"></script>
</body>

</html>
