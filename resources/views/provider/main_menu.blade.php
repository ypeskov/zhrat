<div class="row">
    <div class="main_menu_item">
        <a href="/" class="btn-lg btn-primary">{{ Lang::ucFirst('app.home') }}</a>
    </div>

    <div class="main_menu_item">
        <a href="/orders" class="btn-lg btn-primary">{{ Lang::ucfirst('app.orders') }}</a>
    </div>
</div>