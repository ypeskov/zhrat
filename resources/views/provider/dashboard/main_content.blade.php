<div class="row">
    <table id="dashboard_main_table">

        @foreach($orders as $order)
            <tr>
                <td>{{ $order['label'] }}</td>
                <td><strong>{{ $order['quantity'] }}</strong></td>
                <td><button>{{ Lang::ucFirst('app.go') }}</button></td>
            </tr>
        @endforeach

    </table>
</div>