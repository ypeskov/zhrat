@extends('provider.layouts.main')

@section('content')
    @include('provider.header')

    <div id="menu_and_content_section" class="row">
        <div id="menu_section" class="col-xs-2">
            @include('provider.main_menu')
        </div>

        <div id="main_content_section" class="col-xs-10">
            @include('provider.dashboard.main_content')
        </div>
    </div>
@stop

