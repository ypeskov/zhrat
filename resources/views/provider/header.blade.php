<div id="provider_header_wrapper" class="row">
    <div id="banner_wrapper" class="col-xs-6">

    </div>

    <div id="user_section_wrapper" class="col-xs-6">
        <div class="row">
            <div class="col-xs-offset-9 col-xs-3">
                <button id="logout_button">{{ Lang::ucFirst('app.logout') }}</button>
            </div>
        </div>

        <div class="row">
            <span>{{ Lang::ucFirst('app.email') }}:&nbsp;{{ $user->email }}</span>
        </div>

        <div class="row">
            <span>{{ Lang::ucFirst('app.provider') }}:&nbsp;{{ $user->provider->provider_full_name }}</span>
        </div>
    </div>
</div>